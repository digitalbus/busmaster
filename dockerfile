### STAGE 1: Build ###
FROM node:10.16-alpine as build
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY ./package.json .
RUN npm install
RUN npm install react-scripts -g
COPY . .
RUN npm run build

### STAGE 2: Production Environment ###
FROM nginx:1.13.12-alpine
COPY --from=build ./build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]