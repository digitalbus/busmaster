import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'semantic-ui-css/semantic.min.css'
import Home from './pages/Home';
import Login from './pages/Login'
import ManageBus from './pages/ManageBus';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { createBrowserHistory } from 'history';
import ManageTimetable from './pages/ManageTimetable';
import BusInput from './components/BusInput';
import BusAdd from './components/BusAdd'
import ManageQueue from './pages/ManageQueue'
import QueueOption from './pages/QueueOption'
import TimetableAdd from './components/TimetableAdd';
import TimetableInput from './pages/TimetableInput'
import Header from './components/Header'
import Sidebar from './components/Sidebar'
import './pages/style.css'
import 'react-notifications/lib/notifications.css';
import Dashboard from './pages/Dashboard'
import ExportData from './pages/ExportData'
import ManageDriver from './pages/ManageDriver'
import DriverInput from './components/DriverInput'
import Approve from "./pages/Approve"
import ManageSemester from './pages/ManageSemester'

const browserHistory = createBrowserHistory()


function App() {
  return (
    <Router history={browserHistory}>
      <Route path="/login" exact component={Login} />
      <div>
        <Header />
        <div className="main-content">
          <Sidebar history={browserHistory}/>
          <div className="scrollable-content">
            <Route path="/" exact component={Dashboard} />
            <Route path="/approve" exact component={Approve} />
            <Route path="/bus" component={ManageBus} />
            <Route path="/timetable" component={ManageTimetable} />
            <Route path="/businput/:id" component={BusInput} />
            <Route path="/busadd" component={BusAdd} />
            <Route path="/queue" component={ManageQueue} />
            <Route path="/queueoption/:id" component={QueueOption} />
            <Route path="/timetableinput/:id" component={TimetableInput} />
            <Route path="/timetableadd" component={TimetableAdd} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/data" component={ExportData} />
            <Route path="/driver" component={ManageDriver} />
            <Route path="/driverinput/:id" component={DriverInput} />
            <Route path="/semester" component={ManageSemester} />
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
