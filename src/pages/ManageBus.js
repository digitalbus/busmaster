import React from 'react';
import BusList from '../components/BusList';
import Header from '../components/Header'
import Menubutton from '../components/Menubutton'
import { Link } from 'react-router-dom'
import BusInput from '../components/BusInput';
import { Icon, Button, Sidebar, Container } from 'semantic-ui-react'
import BusAdd from '../components/BusAdd'


export default class ManageBus extends React.Component {

  state = {
    isShowModal: false
  }

  handleToggleModal = () => {
    this.setState({
      isShowModal: !this.state.isShowModal,
    })
  }


  render = () => {
    return (
      <div>
        <Container>
          < h1 style={{ margin: '20px' }}>
            <Icon
              name='car'
              style={{ float: 'left', color: 'salmon' }}
            />
            การจัดการรถรับส่ง
          </h1>

          <Button
            icon labelPosition='left'
            style={{ float: 'right',margin:'10px' }}
            onClick={this.handleToggleModal}
          >
            <Icon name='plus square outline' />
            เพิ่มข้อมูลรถรับส่ง
            </Button>
          <BusAdd targetRouteId={this.state.routeId} 
                  isShowModal={this.state.isShowModal} 
                  toggleModal={this.handleToggleModal}
          />
        </Container>
        <BusList />
      </div >
    )
  }
}

