
import React from 'react'
import { Dropdown, Grid, Input, TextArea } from 'semantic-ui-react'

const linkOption = [{
    text: 'Dashboard',
    value: `https://kibana.bourney.ml/app/kibana#/dashboard/b48075f0-f948-11e9-a029-0bb5b93d41ce?embed=true&_g=(filters:!())&_a=(description:'',filters:!(),fullScreenMode:!f,options:(hidePanelTitles:!f,useMargins:!t),panels:!((embeddableConfig:(mapCenter:!(13.734049042440066,100.47615051269533),mapZoom:11),gridData:(h:15,i:'1',w:48,x:0,y:0),id:'02d52cb0-f8a8-11e9-a029-0bb5b93d41ce',panelIndex:'1',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'2',w:17,x:31,y:29),id:'4eeb42f0-f958-11e9-a029-0bb5b93d41ce',panelIndex:'2',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'3',w:17,x:31,y:42),id:'7dd06b40-f958-11e9-a029-0bb5b93d41ce',panelIndex:'3',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'4',w:31,x:0,y:29),id:'911b6dd0-f94e-11e9-a029-0bb5b93d41ce',panelIndex:'4',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'5',w:31,x:0,y:42),id:a162e8d0-f94e-11e9-a029-0bb5b93d41ce,panelIndex:'5',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'6',w:31,x:0,y:55),id:ed2042e0-f95d-11e9-a029-0bb5b93d41ce,panelIndex:'6',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'7',w:31,x:0,y:68),id:'386e3720-f95e-11e9-a029-0bb5b93d41ce',panelIndex:'7',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'8',w:17,x:31,y:55),id:'94efb410-f95e-11e9-a029-0bb5b93d41ce',panelIndex:'8',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'9',w:17,x:31,y:68),id:aabb6280-f95e-11e9-a029-0bb5b93d41ce,panelIndex:'9',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'10',w:17,x:31,y:81),id:'60d4d430-f963-11e9-a029-0bb5b93d41ce',panelIndex:'10',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'11',w:31,x:0,y:81),id:'01316150-f964-11e9-a029-0bb5b93d41ce',panelIndex:'11',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:14,i:'12',w:27,x:10,y:15),id:'420e37d0-fb34-11e9-a029-0bb5b93d41ce',panelIndex:'12',type:visualization,version:'7.2.0')),query:(language:kuery,query:''),timeRestore:!f,title:Dashboard,viewMode:view)`
},
{
    text: 'BMD Visualization',
        value: `https://kibana.bourney.ml/app/kibana#/dashboard/de6cbae0-f948-11e9-a029-0bb5b93d41ce?embed=true&_g=(filters:!())&_a=(description:'',filters:!(),fullScreenMode:!f,options:(hidePanelTitles:!f,useMargins:!t),panels:!((embeddableConfig:(),gridData:(h:13,i:'2',w:48,x:0,y:8),id:be517b00-f958-11e9-a029-0bb5b93d41ce,panelIndex:'2',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'3',w:48,x:0,y:21),id:dd51e1c0-f958-11e9-a029-0bb5b93d41ce,panelIndex:'3',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'4',w:48,x:0,y:34),id:eacecb10-f958-11e9-a029-0bb5b93d41ce,panelIndex:'4',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'5',w:48,x:0,y:47),id:'0f745ac0-f959-11e9-a029-0bb5b93d41ce',panelIndex:'5',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'6',w:48,x:0,y:60),id:'1de78af0-f959-11e9-a029-0bb5b93d41ce',panelIndex:'6',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'7',w:48,x:0,y:73),id:'29746410-f959-11e9-a029-0bb5b93d41ce',panelIndex:'7',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'8',w:48,x:0,y:86),id:'360ba850-f959-11e9-a029-0bb5b93d41ce',panelIndex:'8',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:8,i:'9',w:18,x:6,y:0),id:c4779e50-f959-11e9-a029-0bb5b93d41ce,panelIndex:'9',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:8,i:'10',w:14,x:24,y:0),id:a67510d0-f95f-11e9-a029-0bb5b93d41ce,panelIndex:'10',type:visualization,version:'7.2.0')),query:(language:kuery,query:''),timeRestore:!f,title:'BMD+Visualization',viewMode:view)`
    },
{
    text: 'BKT Visualization',
        value: `https://kibana.bourney.ml/app/kibana#/dashboard/6963cd10-f957-11e9-a029-0bb5b93d41ce?embed=true&_g=(filters:!())&_a=(description:'',filters:!(),fullScreenMode:!f,options:(hidePanelTitles:!f,useMargins:!t),panels:!((embeddableConfig:(),gridData:(h:8,i:'2',w:13,x:4,y:0),id:ffe69d60-f959-11e9-a029-0bb5b93d41ce,panelIndex:'2',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:14,i:'3',w:48,x:0,y:8),id:'9a9c2cd0-f95a-11e9-a029-0bb5b93d41ce',panelIndex:'3',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'4',w:48,x:0,y:22),id:be3a1080-f95a-11e9-a029-0bb5b93d41ce,panelIndex:'4',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:14,i:'5',w:48,x:0,y:35),id:e9e08890-f95a-11e9-a029-0bb5b93d41ce,panelIndex:'5',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:14,i:'6',w:48,x:0,y:49),id:f4a5f300-f95a-11e9-a029-0bb5b93d41ce,panelIndex:'6',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:14,i:'7',w:48,x:0,y:63),id:'053e9b40-f95b-11e9-a029-0bb5b93d41ce',panelIndex:'7',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'8',w:48,x:0,y:77),id:'196aca80-f95b-11e9-a029-0bb5b93d41ce',panelIndex:'8',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:13,i:'9',w:48,x:0,y:90),id:'226782e0-f95b-11e9-a029-0bb5b93d41ce',panelIndex:'9',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:8,i:'10',w:13,x:30,y:0),id:f03f4b40-f95f-11e9-a029-0bb5b93d41ce,panelIndex:'10',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:8,i:'11',w:13,x:17,y:0),id:d4a82730-f95f-11e9-a029-0bb5b93d41ce,panelIndex:'11',type:visualization,version:'7.2.0')),query:(language:kuery,query:''),timeRestore:!f,title:'BKT+Visualization',viewMode:view)`
    },
{
    text: 'KNX Visualization',
        value: `https://kibana.bourney.ml/app/kibana#/dashboard/7c09b0d0-f95f-11e9-a029-0bb5b93d41ce?embed=true&_g=(filters:!())&_a=(description:'',filters:!(),fullScreenMode:!f,options:(hidePanelTitles:!f,useMargins:!t),panels:!((embeddableConfig:(),gridData:(h:11,i:'1',w:15,x:9,y:0),id:'55495770-f95f-11e9-a029-0bb5b93d41ce',panelIndex:'1',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:11,i:'2',w:15,x:24,y:0),id:'5ce98d10-f95f-11e9-a029-0bb5b93d41ce',panelIndex:'2',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:15,i:'4',w:24,x:0,y:11),id:cc602360-f960-11e9-a029-0bb5b93d41ce,panelIndex:'4',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:15,i:'5',w:24,x:24,y:11),id:d60354f0-f960-11e9-a029-0bb5b93d41ce,panelIndex:'5',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:15,i:'6',w:24,x:0,y:26),id:'012337e0-f961-11e9-a029-0bb5b93d41ce',panelIndex:'6',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:15,i:'7',w:24,x:24,y:26),id:'14aa9ba0-f961-11e9-a029-0bb5b93d41ce',panelIndex:'7',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:15,i:'8',w:24,x:0,y:41),id:'25d87f00-f961-11e9-a029-0bb5b93d41ce',panelIndex:'8',type:visualization,version:'7.2.0')),query:(language:kuery,query:''),timeRestore:!f,title:'KNX+Visualization',viewMode:view)`
    },
{
    text: 'TLT Visualization',
        value: `https://kibana.bourney.ml/app/kibana#/dashboard/7d114050-f960-11e9-a029-0bb5b93d41ce?embed=true&_g=(filters:!())&_a=(description:'',filters:!(),fullScreenMode:!f,options:(hidePanelTitles:!f,useMargins:!t),panels:!((embeddableConfig:(),gridData:(h:9,i:'1',w:14,x:14,y:0),id:'69429a60-f960-11e9-a029-0bb5b93d41ce',panelIndex:'1',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:15,i:'2',w:24,x:0,y:9),id:dabaf750-f965-11e9-a029-0bb5b93d41ce,panelIndex:'2',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:15,i:'3',w:24,x:24,y:9),id:e9e56990-f965-11e9-a029-0bb5b93d41ce,panelIndex:'3',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:15,i:'4',w:24,x:24,y:24),id:f22b2e00-f965-11e9-a029-0bb5b93d41ce,panelIndex:'4',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:15,i:'5',w:24,x:0,y:24),id:'05ca1160-f966-11e9-a029-0bb5b93d41ce',panelIndex:'5',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:15,i:'6',w:24,x:0,y:39),id:'0f8fbf10-f966-11e9-a029-0bb5b93d41ce',panelIndex:'6',type:visualization,version:'7.2.0')),query:(language:kuery,query:''),timeRestore:!f,title:'TLT+Visualization',viewMode:view)`
    },
{
    text: '2018 Visualization',
        value: `https://kibana.bourney.ml/app/kibana#/dashboard/0c270a00-fa55-11e9-a029-0bb5b93d41ce?embed=true&_g=(filters:!())&_a=(description:'',filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:e5f90580-f8a7-11e9-a029-0bb5b93d41ce,key:createAt,negate:!f,params:(gte:'2018-01-01',lt:'2018-12-31'),type:range,value:'Jan+1,+2018+@+00:00:00.000+to+Dec+31,+2018+@+00:00:00.000'),range:(createAt:(gte:'2018-01-01',lt:'2018-12-31')))),fullScreenMode:!f,options:(hidePanelTitles:!f,useMargins:!t),panels:!((embeddableConfig:(),gridData:(h:15,i:'1',w:24,x:0,y:0),id:'6bbe3a30-f98b-11e9-a029-0bb5b93d41ce',panelIndex:'1',type:visualization,version:'7.2.0'),(embeddableConfig:(),gridData:(h:15,i:'3',w:24,x:24,y:0),id:'555fe9a0-fa44-11e9-a029-0bb5b93d41ce',panelIndex:'3',type:visualization,version:'7.2.0'),(embeddableConfig:(vis:(colors:(Count:%23F9BA8F),legendOpen:!f)),gridData:(h:18,i:'4',w:48,x:0,y:15),id:e9b168c0-fa4b-11e9-a029-0bb5b93d41ce,panelIndex:'4',type:visualization,version:'7.2.0')),query:(language:kuery,query:''),timeRestore:!f,title:'2018+Dashboard',viewMode:view)`
    },]

export default class Dashboard extends React.Component {

    state = {
        selectedOption: linkOption[0].value
    }

    handleChooseLink = (event, picker) => {
        let { value } = picker
        console.log('this is link value = ', value)
        this.setState({ selectedOption: value })
    }

    render = () => {
        return (
            <div>
                <div>
                    <Dropdown
                        placeholder='Select...'
                        clearable options={linkOption}
                        selection
                        onChange={this.handleChooseLink}
                    />
                </div>
                <iframe src={this.state.selectedOption}
                        height="600"
                        width="1050">
                </iframe>
            </div>

        );
    }
}