import React from 'react'
import {Button, Container, Icon} from 'semantic-ui-react'
import TimetableList from '../components/TimetableList'
import ChooseRoute from '../components/ChooseRoute'
import TimetableAdd from '../components/TimetableAdd'


const selectableDay = [{
        text: 'monday',
        value: 'monday',
    },
    {
        text: 'tuesday',
        value: 'tuesday',
    },
    {
        text: 'wednesday',
        value: 'wednesday',
    },
    {
        text: 'thursday',
        value: 'thursday',
    },
    {
        text: 'friday',
        value: 'friday',
    },
    {
        text: 'saturday',
        value: 'saturday',
    },
    {
        text: 'sunday',
        value: 'sunday',
    }]

const departureOption = [{
        text: 'BMD',
        value: 'BMD',
    },
    {
        text: 'BKT',
        value: 'BKT',
    },
    {
        text: 'KNX',
        value: 'KNX',
    },
    {
        text: 'TLT',
        value: 'TLT',
    }]

export default class ManageTimetable extends React.Component {

    state = {
        chosenDate: new Date(),
        routeList: [{arrival: ''}],
        timetable: undefined,
        arrivalOption: undefined,
        selectedDate: 'monday',
        departurePoint: undefined,
        routeId: 0,
        isShowModal: false,
    }

    componentDidMount = async () => {
        let search = new URLSearchParams(this.props.location.search)
        let departure = search.get("dep")
        let arrival = search.get("arv")
        if (arrival) {
            let response = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/timetable/all/${arrival}?day=${this.state.selectedDate}`)
            response = await response.json()
            this.setState({timetable: response, routeId: arrival, departurePoint: departure})
        }
        console.log(this.state)

    }

    departureChange = async (event, picker) => {
        let value = picker.value
        this.setState({departurePoint: value})
        this.props.history.push({
            pathname: '/timetable',
            search: `?dep=${value}`
        })
        let response = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/route/departure?departureAt=${value}`)
        let data = await response.json()
        await this.changeToOption(data, 'arrivalOption')
    }

    handleChooseRoute = async (event, picker) => {
        let {value} = picker
        this.props.history.push({
            pathname: '/timetable',
            search: `${this.props.location.search}&arv=${value}`
        })
        await this.setState({routeId: value})
        await this.fetchTimetable()
    }

    fetchTimetable = async () => {
        let response = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/timetable/all/${this.state.routeId}?day=${this.state.selectedDate}`)
        const data = await response.json()
        await this.setState({timetable: data})
        console.log('fetch', data)
    }

    changeToOption = (routeList, state) => {
        const option = []
        routeList.map(route => {
            option.push({
                text: route.arrival,
                value: route.id,
                ...route
            })
        })
        this.setState({[state]: option})
    }

    handleChooseDay = async (event, picker) => {
        let {value} = picker
        await this.setState({selectedDate: value})
        await this.fetchTimetable()
    }

    handleToggleModal = () => {
        this.setState({
            isShowModal: !this.state.isShowModal,
        })
    }

    render = () => {
        return (
            <div>
                <Container>
                    <h1 style={{margin: '20px'}}>
                        <Icon
                            name='calendar alternate outline'
                            style={{float: 'left', color: 'salmon'}}
                        />
                        การจัดการตารางเวลา
                    </h1>

                    <ChooseRoute
                        handleDeparterChange={this.departureChange}
                        departureOption={departureOption}
                        handleChooseRoute={this.handleChooseRoute}
                        arrivalOption={this.state.arrivalOption}
                        dayOption={selectableDay}
                        dayValue={this.state.selectedDate}
                        handleChooseDay={this.handleChooseDay}
                    />

                    {this.state.timetable ? (
                            <>
                                <Button
                                    icon labelPosition='left'
                                    style={{float: 'right', margin: '20px'}}
                                    onClick={this.handleToggleModal}
                                >
                                    <Icon name='plus square outline'/>
                                    เพิ่มข้อมูลตารางเวลา
                                </Button>
                                <TimetableAdd targetRouteId={this.state.routeId}
                                              isShowModal={this.state.isShowModal}
                                              toggleModal={this.handleToggleModal}
                                              fetchTimetable={() => this.fetchTimetable()}/>
                            </>
                        ) :
                        (
                            <div></div>
                        )
                    }
                </Container>

                <TimetableList fetchTimetable={() => this.fetchTimetable()} timetables={this.state.timetable}/>
            </div>
        )
    }
}

