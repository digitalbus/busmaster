import React from 'react'
import styled from 'styled-components'
import {Dropdown, Grid, Input, TextArea} from 'semantic-ui-react'
import moment from 'moment-timezone'

const ColumnStyled = styled(Grid.Column)`
    display: flex;
    margin-top: -30px;
`

export default class extends React.Component {

    async componentWillMount() {
        let busDriver = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/bus_driver`)
        busDriver = await busDriver.json()
        let buses = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/bus`)
        buses = await buses.json()
        let {trip} = this.props
        if (trip) {
            this.setState({
                driverId: trip.driver.id,
                busType: trip.bus.busType,
                seat: trip.bus.seat,
                busId: trip.bus.id,
                tripId: trip.id,
                timetableId : trip.timetable.id
            })
        }
        this.setState({bus: buses, driver: busDriver})
    }


    constructor(props) {
        super(props)
        console.log('propssBusStamppp', this.props)
        this.state = {
            driver: [],
            bus: [],
            busType: undefined,
            seat: undefined,

            busId: undefined,
            driverId: undefined,
            timetableId: undefined,
            busStamp: undefined
        }
    }

    handleSaveToDatabase = async (event) => {
        const time = await moment().tz('Asia/Bangkok').format()
        let {busId, driverId, tripId,timetableId} = this.state
        let {timetable, fetchAnyTrip} = this.props
        this.setState({busStamp: time})
        let preparedTrip = {}
        if (tripId) {
            preparedTrip = {
                "id": tripId,
                "bus": {"id": busId},
                "driver": {"id": driverId},
                "timetable": {"id": timetableId},
                "busStamp": time
            }
        } else {
            preparedTrip = {
                "bus": {"id": busId},
                "driver": {"id": driverId},
                "timetable": {"id": timetable.id},
                "busStamp": time
            }
        }
        console.log('PREPAREDTRIP', preparedTrip)
        let res = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/trip/stampBus`, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(preparedTrip)
        })
        res = await res.json()
        fetchAnyTrip(timetableId)
        await console.log('FinalRes*', res)
        await console.log('TIMEEEE*', time)
    }


    handleDriverChange = (event, picker) => {
        let nameDriver = picker.value
        this.setState({driverId: nameDriver})
        console.log(nameDriver)
    }

    handleBusTypeChange = (event, picker) => {
        let busType = picker.value
        this.setState({busType: busType})
    }

    handlelicencePlateChange = async (event, picker) => {
        let licencePlate = picker.value
        this.setState({busId: licencePlate})
    }

    handleSeatChange = (e) => {
        let seat = e.target.value
        let trip = this.state.trip
        trip.seat = seat
        this.setState({trip})
        console.log(this.state.trip)
    }

    showTimeStamp = (e) => {
        const time = moment().format('MMMM Do YYYY, h:mm:ss a')
        this.setState({busStamp: time})
    }

    driverDropdown = () => {
        let list = []
        this.state.driver.map(value => {
            list.push({key: value.id, text: value.firstName, value: value.id})
        })
        return <Dropdown
            placeholder='Select...'
            clearable options={list}
            selection
            value={this.state.driverId}
            onChange={this.handleDriverChange}/>
    }

    busDropdown = () => {
        let list = []
        this.state.bus.map(value => {
            list.push({key: value.id, text: value.licencePlate, value: value.id})
        })
        return <Dropdown
            placeholder='Select...'
            selection
            search
            options={list}
            value={this.state.busId}
            onChange={this.getBus}
        />
    }

    getBus = (event, {value}) => {
        console.log(value)
        this.state.bus.map(data => {
            if (data.id == value) {
                this.setState({busType: data.busType, seat: data.seat, busId: value})
            }
        })
    }

    render = () => (
        <div>
            <Grid columns={2} style={{width: '540px'}}>
                <ColumnStyled first>
                    <p style={{fontSize: '18px', justifySelf: 'center', marginTop: '35px', marginLeft: '10px'}}>
                        ชื่อคนขับ:
                    </p>
                </ColumnStyled>
                <ColumnStyled style={{marginTop: '5px'}}>
                    {this.driverDropdown()}
                </ColumnStyled>


                <ColumnStyled>
                    <p style={{fontSize: '18px', justifySelf: 'center', marginTop: '10px', marginLeft: '10px'}}>
                        ทะเบียนรถ:
                    </p>
                </ColumnStyled>
                <ColumnStyled style={{marginTop: '1px'}}>
                    {this.busDropdown()}
                </ColumnStyled>

                <ColumnStyled>
                    <p style={{fontSize: '18px', justifySelf: 'center', marginLeft: '10px'}}>
                        ประเภทของรถ:
                    </p>
                </ColumnStyled>
                <ColumnStyled>
                    <Input style={{marginTop: '12px'}} value={this.state.busType}/>
                </ColumnStyled>


                <ColumnStyled>
                    <p style={{fontSize: '18px', justifySelf: 'center', marginTop: '5px', marginLeft: '10px'}}>
                        จำนวนที่นั่ง:
                    </p>
                </ColumnStyled>
                <ColumnStyled>
                    <Input style={{marginTop: '12px'}} value={this.state.seat}/>
                </ColumnStyled>

                <ColumnStyled>
                    <p style={{fontSize: '18px', justifySelf: 'center', marginTop: '10px', marginLeft: '10px'}}>
                        Bus Stamp:
                    </p>
                </ColumnStyled>
                <ColumnStyled>
                    <TextArea style={{marginTop: '12px'}}
                              value={this.state.busStamp}
                    />
                </ColumnStyled>

                <ColumnStyled>
                </ColumnStyled>
                <ColumnStyled>
                    <button style={{
                        float: 'right',
                        margin: '20px',
                        fontSize: '15px',
                        padding: '10px',
                        borderWidth: '2px',
                        borderRadius: '8px',
                        backgroundColor: 'salmon'
                    }}
                            onClick={this.handleSaveToDatabase}

                    >
                        ออกรถ
                    </button>
                </ColumnStyled>
            </Grid>
        </div>

    )
}
