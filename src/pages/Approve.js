import React from 'react'
import RequestQueueList from '../components/RequestQueueList'
import {Button, Container, Icon} from 'semantic-ui-react'
import Swal from 'sweetalert2'
import TripModal from "../components/Modal/TripModal"

class Approve extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            requestQueues: [],
            isShowModal: false,

        }
    }

    componentDidMount = async () => {
        await this.fetch()
    }

    selectTimetable = (timetable) => {
        this.setState({selectedTimetable:timetable,isShowModal: true})
    }

    handleToggleModal = () => {
        this.setState({
            isShowModal: !this.state.isShowModal,
        })
    }

    fetch = async () => {
        let requestQueues = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/queue/request`)
        requestQueues = await requestQueues.json()
        this.setState({requestQueues: requestQueues})
    }

    approveQueue = async (timetable) => {
        let seat = 0
        Swal.mixin({
            input: 'text',
            confirmButtonText: 'ตกลง',
            showCancelButton: true,
            progressSteps: ['1']
        }).queue([
            {
                title: 'คุณต้องการอนุมัติคิวพิเศษหรือไม่',
                text: 'โปรดระบุจำนวนคนและจำเป็นต้องมีรถรองรับ'
            }
        ]).then((result) => {
            let seat = result.value
            if (seat) {
                fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/queue/approve?timetable_id=${timetable.id}&seat=${seat}`)
                Swal.fire({
                    title: 'สำเร็จ',
                    type: 'success',
                    timer: 1500
                }).then(e => this.fetch())
            }
        })
    }

    rejectQueue = async (timetable) => {
        Swal.fire({
            title: 'คุณต้องการลบรายการนี้ใช่หรือไม่ ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then(async (result) => {
            if (result.value) {
                let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/queue/reject?timetable_id=${timetable.id}`, {
                    method: 'DELETE'
                })
                if (data.ok) {
                    Swal.fire(
                        'ยกเลิกสำเร็จ!',
                        'success'
                    )
                    this.fetch()
                }
            }
        })
    }

    render() {
        return (
            <Container>
                <h1 style={{margin: '20px'}}>
                    <Icon name='user plus' style={{float: 'left', color: 'salmon'}}/> การจัดการคิวพิเศษ</h1>
                <RequestQueueList selectTimetable={this.selectTimetable} approve={this.approveQueue} cancel={this.rejectQueue} fetch={this.fetch}
                                  requestQueues={this.state.requestQueues}/>
                {this.state.selectedTimetable && (<TripModal busChange driverChange timetable={this.state.selectedTimetable} fetch={this.fetch} toggleModal={this.handleToggleModal} isShowModal={this.state.isShowModal}/>)}
            </Container>
        )
    }
}

export default Approve
