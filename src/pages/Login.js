import React from 'react'
import Group from '../assets/img/Group.png'
import {Button, Card, Dimmer, Input, Loader} from 'semantic-ui-react'

export default class extends React.Component {
    state = {
        loading: false
    }

    handleInputChange = (e) => {
        let {state} = this
        let {value, name} = e.target
        this.setState({...state, [name]: value})
    }

    handleLogin = async () => {
        this.setState({loading: true})
        let {username, password} = this.state
        if (username && password) {
            try {
                let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/user/login`, {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({
                        username,
                        password
                    })
                })
                if (data.status == 404) {

                    this.setState({loading: false, errorMessage: 'username or password not match'})
                } else {
                    data = await data.json()
                    localStorage.setItem("user", JSON.stringify(data))
                    this.props.history.push('/dashboard')
                }

                console.log(data)
            } catch (error) {
                console.log(error)
            }
        }
    }


    render = () => {
        return (
            <div style={divStyle}>
                {this.state.loading ?
                    <Dimmer active>
                        <Loader content='Loading'/>
                    </Dimmer>
                    :
                    null

                }
                <Card style={{
                    textAlign: 'center',
                    width: '70%',
                    height: '70%',
                    borderRadius: '20px',
                    display: 'flex',
                    justifyContent: 'center'
                }}>
                    <div style={{
                        flexDirection: 'row',
                        display: 'flex',
                        justifyContent: 'space-around',
                        marginLeft: '5%'
                    }}>
                        <div>
                            <img src={Group}
                                 style={{width: '350px', textAlign: 'left'}}
                            />
                            <div style={{fontWeight: 900, fontSize: '20px', marginTop: '10%', letterSpacing: '4px'}}>
                                BOURNEY
                            </div>
                        </div>


                        <div style={{marginRight: '10%', marginTop: '5%'}}>
                            <div style={{marginBottom: '20%', fontWeight: 'bold', fontSize: '20px', marginLeft: '13%'}}>
                                Bus Master Login
                            </div>
                            <p style={{color: 'red'}}>{this.state.errorMessage}</p>
                            <div style={{margin: '8px 0px 8px 0px'}}>
                                <Input name={'username'}
                                       onChange={this.handleInputChange}
                                       placeholder={'username'}
                                       style={{width: '120%'}}
                                />
                            </div>
                            <div>
                                <Input style={{width: '120%'}}
                                       type={'password'}
                                       name={'password'}
                                       onChange={this.handleInputChange}
                                       placeholder={'password'}
                                />
                            </div>
                            <Button onClick={this.handleLogin}
                                    style={{
                                        marginTop: '20px', color: 'white', width: '120%',
                                        background: 'linear-gradient(to right, #fda90a 0%, #fe4f78 100%)'
                                    }}>
                                LOGIN
                            </Button>
                        </div>
                    </div>
                </Card>
            </div>
        )
    }
}

const divStyle = {
    background: 'linear-gradient(to bottom, #fda90a 0%, #fe4f78 100%)',
    textAlign: 'center',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    WebkitBoxShadow: '10px 10px 24px -16px rgba(0,0,0,0.75)',
    MozBoxShadow: ' 10px 10px 24px -16px rgba(0,0,0,0.75)',
    boxShadow: ' 10px 10px 24px -16px rgba(0,0,0,0.75)',
}