import React from 'react'
import {Button, Container, Icon} from 'semantic-ui-react'
import ChooseRoute from '../components/ChooseRoute'
import TimetbleListForQueue from '../components/TimetbleListForQueue'

export default class ManageQueue extends React.Component {

    state = {
        chosenDate: new Date(),
        routeList: [{arrival: ''}],
        timetable: undefined,
        arrivalOption: undefined,
        seletedDay: 'monday',
        dep: undefined,
        arv: undefined
    }

    componentDidMount = async () => {
        let search = new URLSearchParams(this.props.location.search)
        let departure = await search.get("dep")
        let arrival = await search.get("arv")
        if (arrival) {
            console.log('dep&ARV', departure + arrival)
            await this.setState({departurePoint: departure, routeId: arrival})
            this.fetchData()
        }
    }

    handleDeparterChange = async (event, picker) => {
        let value = picker.value
        this.setState({departurePoint: value})
        this.props.history.push({
            pathname: '/queue',
            search: `dep=${value}`
        })
        let response = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/route/departure?departureAt=${value}`)
        let data = await response.json()
        await this.changeToOption(data, 'arrivalOption')
    }

    handleChooseRoute = async (event, picker) => {
        let {value} = picker
        this.props.history.push({
            pathname: '/queue',
            search: `${this.props.location.search}&arv=${value}`
        })
        this.setState({routeId: value})
    }

    fetchData = async () => {
        let url
        if (this.state.seletedDay && this.state.routeId) {
            if (this.state.routeId == "ALL") {
                url = `${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/timetable?day=${this.state.seletedDay}&dep=${this.state.departurePoint}`
            } else {
                url = `${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/timetable/routebytime/${this.state.routeId}?day=${this.state.seletedDay}`
            }
            let response = await fetch(url)
            response = await response.json()
            this.setState({timetable: response})
        }
    }

    changeToOption = (routeList, state) => {
        const option = []
        option.push({
            text: "ALL",
            value: "ALL",
        })
        routeList.map(route => {
            option.push({
                text: route.arrival,
                value: route.id,
                ...route
            })
        })
        this.setState({[state]: option})
    }

    handleChooseDay = async (event, picker) => {
        let {value} = picker
        console.log('handleChooseDay', value)
        this.setState({seletedDay: value})
    }

    render = () => {
        return (
            <div>
                <Container>
                    <h1 style={{margin: '20px'}}>
                        <Icon name='user circle' style={{float: 'left', color: 'salmon'}}/> การจัดการคิว</h1>
                    <ChooseRoute
                        handleDeparterChange={this.handleDeparterChange}
                        departureOption={departureOption}
                        handleChooseRoute={this.handleChooseRoute}
                        arrivalOption={this.state.arrivalOption}
                        dayOption={selectableDay}
                        dayValue={this.state.seletedDay}
                        handleChooseDay={this.handleChooseDay}
                    /> 
                    <div style={{display: 'flex', justifyContent: 'flex-end'}}>
                        <Button primary style={{margin: '5px'}} onClick={this.fetchData}>ค้นหารอบ</Button>
                    </div>
                    <TimetbleListForQueue timetables={this.state.timetable}/>
                </Container>
            </div>

        )
    }

}

const departureOption = [{
    text: 'BMD',
    value: 'BMD',
},
    {
        text: 'BKT',
        value: 'BKT',

    },
    {
        text: 'KNX',
        value: 'KNX',

    },
    {
        text: 'TLT',
        value: 'TLT',

    }]

const selectableDay = [{
    text: 'monday',
    value: 'monday',

},
    {
        text: 'tuesday',
        value: 'tuesday',

    },
    {
        text: 'wednesday',
        value: 'wednesday',

    },
    {
        text: 'thursday',
        value: 'thursday',

    },
    {
        text: 'friday',
        value: 'friday',

    },
    {
        text: 'saturday',
        value: 'saturday',

    },
    {
        text: 'sunday',
        value: 'sunday',

    }]
