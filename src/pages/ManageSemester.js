import React from 'react';
import { Icon, Container, Button } from 'semantic-ui-react'
import { Tab } from 'semantic-ui-react'
import PresetAdd from '../components/PresetAdd'
import PresetList from '../components/PresetList';


export default class ManageBus extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            preset: []
        }
    }

    componentDidMount = async () => {
        this.fetchData()
    }

    fetchData = async () => {
        let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/preset`)
        let list = await data.json()
        this.setState({preset: list})
    }

    handleToggleModal = () => {
        this.setState({
            isShowModal: !this.state.isShowModal
        })
    }

    render = () => {
        return (
            <div>
                <Container>
                    < h1 style={{ margin: '20px' }}>
                        <Icon
                            name='calendar check outline'
                            style={{ float: 'left', color: 'salmon' }}
                        />
                        การตั้งค่าตารางเวลา
                    </h1>
                    <Button
                        icon labelPosition='left'
                        style={{ float: 'right', margin: '20px' }}
                        onClick={this.handleToggleModal}
                    >
                        <Icon name='plus square outline' />
                        จัดกลุ่มตารางเวลา
                    </Button>
                    <PresetAdd
                        isShowModal={this.state.isShowModal}
                        toggleModal={this.handleToggleModal}
                        fetchData={this.fetchData}
                    />
                    <h3>
                        กลุ่มตารางเวลาทั้งหมด
                    </h3>
                </Container>
                <PresetList presets={this.state.preset} fetchData={this.fetchData}/>
            </div>
        )
    }
}