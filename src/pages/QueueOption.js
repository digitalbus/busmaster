import React from 'react'
import {Card, Container, Dropdown, Icon} from 'semantic-ui-react'
import CheckInBookedQueue from "../components/CheckInQueue/CheckInBookedQueue"
import CheckInRequestQueue from "../components/CheckInQueue/CheckInRequestQueue"

export default class extends React.Component {

    state = {
        loading: true,
        selectedTripId: 0,
        requestedQueue: [{
            text: 'คิวทั่วไป',
            value: 0
        }],
        timetable: {route: {}}
    }

    componentDidMount = async () => {
        let {id} = this.props.match.params
        let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/timetable/${id}`)
        let requestedTrip = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/trip/ongoing/timetable/${id}`)
        data = await data.json()
        requestedTrip = await requestedTrip.json()
        this.setState({loading: false, timetable: data, trip: requestedTrip})
        this.convertTripToDropdown()
    }

    convertTripToDropdown = () => {
        const {requestedQueue, trip} = this.state
        trip.map(t => {
            requestedQueue.push({
                text: `คิวพิเศษ ${t.bus.licencePlate}`,
                value: t.id
            })
        })
        this.setState({requestedQueue})
    }

    renderOption() {
        let {timetable, selectedTripId} = this.state
        if (selectedTripId == 0 && timetable.id) {
            return <CheckInBookedQueue timetable={timetable}/>
        } else if(selectedTripId > 0) {
            return <CheckInRequestQueue tripId={selectedTripId}/>
        }else{
            return <div> loading.... </div>
        }
    }

    changeTrip = (e, picker) => {
        this.setState({selectedTripId:picker.value})
        console.log('picker', picker.value)
    }

    render = () => {
        let {id} = this.props.match.params
        return (
            <div>
                <Container>
                    <div>
                        <Card style={{width: '50%'}}>
                            <Card.Content
                                style={{
                                    color: 'Black',
                                    fontSize: '18px',
                                    fontStyle: 'Solid',
                                    backgroundColor: '#58D68D'
                                }}>
                                <Dropdown value={this.state.selectedTripId} onChange={this.changeTrip} options={this.state.requestedQueue}/>
                                <div style={{
                                    width: '100%', display: 'flex',
                                    flexDirection: 'row',
                                    flexWrap: 'nowrap',
                                    justifyContent: 'space-between',
                                    alignItems: 'stretch',
                                    alignContent: 'stretch'
                                }}>
                                    <div>
                                        {this.state.timetable.route.departure} => {this.state.timetable.route.arrival}
                                    </div>
                                    <div style={{color: 'black'}}>
                                        {this.state.time}
                                    </div>
                                </div>
                            </Card.Content>
                            <Card.Content style={{fontSize: '15px', color: 'Gray', justifyContent: 'space-between'}}>
                                <Icon name='bus'/> {this.state.timetable.busType}
                                <Icon name='book'/> {this.state.timetable.day}
                            </Card.Content>
                        </Card>
                        {this.renderOption()}
                    </div>
                </Container>
            </div>
        )
    }
}
