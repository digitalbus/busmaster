import React from 'react'
import {Button, Container, Icon} from 'semantic-ui-react'
import DriverList from '../components/DriverList'
import DriverAdd from '../components/DriverAdd'

export default class ManageDriver extends React.Component {

    state = {
      isShowModal: false
    }
  
    handleToggleModal = () => {
      this.setState({
        isShowModal: !this.state.isShowModal
      })
    }
  
  
    render = () => {
      return (
        <div>
          <Container>
            < h1 style={{ margin: '20px' }}>
              <Icon
                name='drivers license'
                style={{ float: 'left', color: 'salmon' }}
              />
              การจัดการรายชื่อคนขับ
            </h1>
  
            <Button
              icon labelPosition='left'
              style={{ float: 'right',margin:'10px' }}
              onClick={this.handleToggleModal}
            >
              <Icon name='plus square outline' />
              เพิ่มข้อมูลคนขับ
              </Button>
            <DriverAdd targetRouteId={this.state.routeId} isShowModal={this.state.isShowModal} toggleModal={this.handleToggleModal} />
  
          </Container>
          <DriverList />
        </div >
      )
    }
  }
  