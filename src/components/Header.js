import React from 'react'
import {Header, Icon} from 'semantic-ui-react'
import {Link} from 'react-router-dom'
import {NotificationContainer} from "react-notifications"

export default class extends React.Component{

    constructor(props) {
        super(props)
        this.state={
            passenger:{user:{}}
        }
    }

    componentDidMount = async () => {
        let user = await JSON.parse(await localStorage.getItem('user'))
        if (user) {
            this.setState({passenger: user})
        }
    }


    render() {
        return (
            <Header style={{
                background: 'linear-gradient(to bottom, #fda90a 0%, #fe4f78 100%)',
                padding: '40px',
                // textAlign:'center',
                // fontSize:'25px',
                margin: 0,
                // color:'white',
                borderRadius: '0px 0px 20px 20px',
                display: 'flex'
            }}
                    as='div'>
                <h1 style={{color: 'white', fontSize: 25, textAlign: 'center', width: '100%'}}>
                    ระบบจัดการสำหรับนายท่า
                </h1>
                <Link to='/login'
                      style={{
                          color: 'white',
                          fontSize: '10px',
                          float: 'right',
                          marginTop: '-20px',
                          width: 80,
                          marginRight: '-20px'
                      }}
                >
                    <Icon name='share square outline'/> LOGOUT
                </Link>
                <div style={{
                    float: 'right',
                    fontSize: '10px',
                    right: '10px',
                    marginRight: '20px',
                    position: 'absolute',
                    color: 'white'
                }}>
                    <Icon name='user'/>
                    {this.state.passenger.user.username}
                </div>
                <NotificationContainer/>
            </Header>
        )
    }
}

