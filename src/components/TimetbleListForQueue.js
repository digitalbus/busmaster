import React from 'react'
import { Card, Grid, Icon } from 'semantic-ui-react'
import { Link } from 'react-router-dom'



export default class extends React.Component {

    state = {
        timetableList: []
    }

    constructor(props) {
        super(props)
    }

    handleRequest = (route) => {
        console.log('RouteId = ', route)
        this.props.navigation.navigate('Reserve', { "route": route })
    }

    componentDidMount = async () => {
        console.log('Props', this.props)
        let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/timetable/all`)
        let list = await data.json()
        this.setState({ timetableList: list })
        console.log('listttttt', this.state.timetableList)
    }

    render = () => {
        const timetables = this.props.timetables
        console.log('timetableComponent', timetables)
        if (timetables) {
            return (
                <Grid columns={3} style={{ paddingTop: '33px' }}>
                    {timetables.map(timetable => (
                        <Grid.Column key={timetable.id}>
                            <Link to={`/queueoption/${timetable.id}`}><Card style={{ width: '100%' }}>
                                <Card.Content hearder
                                    style={{ color: 'black', fontSize: '16px', fontStyle: 'Solid', display: 'flex', flexDirection: 'column' }}
                                >
                                    <div >
                                        <p style={{ fontWeight: 'bold', textAlign: 'right', fontSize: '16px', color: '#4C364D', }}>
                                            {timetable.route.departure}
                                            <Icon name='arrow right' />
                                            {timetable.route.arrival}
                                        </p>
                                    </div>
                                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                                        <Icon style={{ color: '#E97E2E' }} name='bus' />{timetable.busType}
                                        <Icon style={{ marginLeft: '20px', color: '#82A0C2' }} name='calendar' />{timetable.day}
                                        <Icon style={{ marginLeft: '20px', color: '#E55151' }} name='user' />{timetable.currentActiveQueue}

                                    </div>

                                </Card.Content>
                                <Card.Content
                                    style={{ color: 'black', paddingTop: '20px', display: 'flex', flexDirection: 'column' }}
                                >
                                    <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
                                        <div style={{ alignSelf: 'center' }}>
                                            <Icon name="clock" style={{ fontSize: '15px' }} />
                                        </div>
                                        <p style={{ fontSize: '23px', textAlign: 'center', alignSelf: 'center' }}>
                                            {timetable.time}
                                        </p>
                                    </div>

                                </Card.Content>
                            </Card></Link>
                        </Grid.Column>
                    ))}
                </Grid>
            )
        } else {
            return <div>
                <Grid columns={3} style={{ paddingTop: '33px' }}>
                    {this.state.timetableList.map(timetable => (
                        <Grid.Column key={timetable.id}>
                            <Link to={`/queueoption/${timetable.id}`}><Card style={{ width: '100%' }}>
                                <Card.Content hearder
                                    style={{ color: 'black', fontSize: '16px', fontStyle: 'Solid', display: 'flex', flexDirection: 'column' }}
                                >
                                    <div >
                                        <p style={{ fontWeight: 'bold', textAlign: 'right', fontSize: '16px', color: '#4C364D', }}>
                                            {timetable.route.departure}
                                            <Icon name='arrow right' />
                                            {timetable.route.arrival}
                                        </p>
                                    </div>
                                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                                        <Icon style={{ color: '#E97E2E' }} name='bus' />{timetable.busType}
                                        <Icon style={{ marginLeft: '20px', color: '#82A0C2' }} name='calendar' />{timetable.day}
                                        <Icon style={{ marginLeft: '20px', color: '#E55151' }} name='user' />{timetable.currentActiveQueue}

                                    </div>

                                </Card.Content>
                                <Card.Content
                                    style={{ color: 'black', paddingTop: '20px', display: 'flex', flexDirection: 'column' }}
                                >
                                    <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
                                        <div style={{ alignSelf: 'center' }}>
                                            <Icon name="clock" style={{ fontSize: '15px' }} />
                                        </div>
                                        <p style={{ fontSize: '23px', textAlign: 'center', alignSelf: 'center' }}>
                                            {timetable.time}
                                        </p>
                                    </div>

                                </Card.Content>
                            </Card></Link>
                        </Grid.Column>
                    ))}
                </Grid>
        </div>
        }
    }

}