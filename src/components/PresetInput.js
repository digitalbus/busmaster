import React from 'react'
import { Input, Icon, Grid, Modal, Table, Checkbox } from 'semantic-ui-react'
import styled, { css } from 'styled-components'

const ColumnStyled = styled(Grid.Column)
    `
    display: flex;
    margin-top: -30px;
`
const BorderInput = styled.input
    `
    border-radius: 8px;
    margin: 30px;
    padding: 5px;

`
const saveButton = css`
    border-color: DodgerBlue;
    background-color: DodgerBlue;
    color: white;
`

const cancelButton = css`
    border-color: lightGray;
    background-color: lightGray;
    color: gray;
`
const BorderButton = styled.button
    `   
    fontSize: 15px;
    padding: 10px;
    border-width: 2px;
    border-style: solid;
    border-radius: 8px;

    ${props => props.save && saveButton || cancelButton}
`

export default class extends React.Component {

    state = {
        preset: {
            namePreset: undefined,
            description: undefined,
            presetTimetables: []
        },
        timetable: [],
        displayTimetable: [],
        namePresetError: false,
        descriptionError: false
    }


    componentDidMount = async () => {
        let param = this.props.targetPresetId
        let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/preset/${param}`)
        let timetables = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/timetable/all`)
        data = await data.json()
        timetables = await timetables.json()
        console.log('dataaaa', data)
        this.setState({ preset: data , timetable: timetables})
        this.calculateConctArray()
        await this.props.fetchData()
    }

    handleNamePresetChange = (e) => {
        let namePreset = e.target.value
        let preset = this.state.preset
        preset.namePreset = namePreset
        this.setState({ preset })
        console.log(this.state.preset)
    }

    handleDescriptionChange = (e) => {
        let description = e.target.value
        let preset = this.state.preset
        preset.description = description
        this.setState({ preset })
        console.log(this.state.preset)
    }

    handleCancel = (e) => {
        console.log('PresetInput Modal',this.props.toggleModal(undefined)())
    }

    handleClick = (selectedTimetable) => {
       let {timetable} = this.state
       timetable.map((d,key) => {
           if(d.id == selectedTimetable.id){
               timetable[key].checked = !d.checked
           }
       })
       this.setState({timetable})
    }

    filterSelectedTime = () => {
        let {timetable} = this.state
        let selectedTimetable = timetable.filter(d => d.checked)
        return selectedTimetable
    }

    handleSave = async (event) => {
        let preset = this.state.preset
        let selectedTimetable = this.filterSelectedTime()
        preset.presetTimetables= selectedTimetable
        console.log('presettttt', preset)
        let res = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/preset`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(preset)
        })
        this.props.toggleModal(undefined)()
    }

    calculateConctArray = () => {
        let {timetable} = this.state
        let {presetTimetables} = this.state.preset
        timetable.map( (t,key) => {
            presetTimetables.map( p => {
                if(t.id == p.id){
                    timetable[key] = {checked: true, ...t}
                }
            })
        })
        this.setState({timetable})
    }


    render = () => (
        <div>
            <Modal open={this.props.isShowModal} onClose={this.props.toggleModal} >
                <Modal.Header>
                    <Icon name='edit outline' style={{ float: 'left', color: '#DB5A6B' }} />
                    แก้ไขข้อมูลกลุ่มตารางเวลา
                        </Modal.Header>
                <Modal.Content>
                    <Modal.Description>
                        <Grid columns={2} style={{ width: '540px' }}>
                            <ColumnStyled>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '30px', marginLeft: '20px' }}>
                                    ชื่อกลุ่มตารางเวลา:
                                        </p>
                            </ColumnStyled>
                            <ColumnStyled>
                                <Input
                                    style={{ margin: '20px', width: '500px' }}
                                    label={{ icon: 'asterisk' }}
                                    labelPosition='right corner'
                                    placeholder='ตารางปิดเทอมเล็ก'
                                    //error={this.state.licencePlateInputError}
                                    value={this.state.preset.namePreset}
                                    onChange={this.handleNamePresetChange} />
                            </ColumnStyled>

                            <ColumnStyled>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '30px', marginLeft: '20px' }}>
                                    รายละเอียด:
                                        </p>
                            </ColumnStyled>
                            <ColumnStyled>
                                <Input
                                    style={{ margin: '20px', width: '500px', height: '100px' }}
                                    label={{ icon: 'asterisk' }}
                                    labelPosition='right corner'
                                    placeholder='ใช้ช่วงปิดเทอมเล็ก ตั้งแต่ 12 ธ.ค. 62 - 15 ม.ค. 62'
                                    //error={this.state.licencePlateInputError}
                                    value={this.state.preset.description}
                                    onChange={this.handleDescriptionChange} />
                            </ColumnStyled>
                            <ColumnStyled>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '30px', marginLeft: '20px' }}>
                                    ตารางเวลาต้องการเลือก:
                                        </p>
                            </ColumnStyled>
                        </Grid>
                        <div>
                            <Table celled compact definition >
                                <Table.Header fullWidth>
                                    <Table.Row style={{ width: '100px' }}>
                                        <Table.HeaderCell>ต้องการเลือก</Table.HeaderCell>
                                        <Table.HeaderCell>สถานที่ต้นทาง</Table.HeaderCell>
                                        <Table.HeaderCell>สถานที่ปลายทาง</Table.HeaderCell>
                                        <Table.HeaderCell>วัน</Table.HeaderCell>
                                        <Table.HeaderCell>เวลา</Table.HeaderCell>
                                        <Table.HeaderCell>ประเภทรถโดยสาร</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {this.state.timetable.map(timetable => (
                                        <Table.Row key={timetable.id}>
                                            <Table.Cell collapsing>
                                                <Checkbox checked={timetable.checked} onClick={() => this.handleClick(timetable)}/>
                                            </Table.Cell>
                                            <Table.Cell>{timetable.route.departure}</Table.Cell>
                                            <Table.Cell>{timetable.route.arrival}</Table.Cell>
                                            <Table.Cell>{timetable.day}</Table.Cell>
                                            <Table.Cell>{timetable.time}</Table.Cell>
                                            <Table.Cell>{timetable.busType}</Table.Cell>
                                        </Table.Row>
                                    ))
                                    }
                                </Table.Body>
                            </Table>
                        </div>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <BorderButton
                        name="cancel"
                        type="reset"
                        onClick={this.handleCancel}
                    >
                        ยกเลิก
                            </BorderButton>
                    <BorderButton
                        name="save"
                        type="submit"
                        onClick={this.handleSave}
                        save
                    >
                        บันทึก
                    </BorderButton>
                </Modal.Actions>
            </Modal>
        </div >
    )
}

