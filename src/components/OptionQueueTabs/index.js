import React from 'react'
import { Tab } from 'semantic-ui-react'
import QrCodePane from './Qrcodepane'
import BusStamp from '../../pages/BusStamp'

const panes = [
  { menuItem: 'Scan QR Code', render: (props) => <Tab.Pane><QrCodePane {...props}/></Tab.Pane> },
  { menuItem: 'Bus Stamp', render: (props) => <Tab.Pane><BusStamp {...props}/></Tab.Pane> },
]

const OptionQueueTabs = (props) => <Tab {...props} panes={panes} menu={{ secondary: true, pointing: true }}/>

export default OptionQueueTabs
