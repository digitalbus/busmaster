import React from 'react'
import ScanQRcode from '../../pages/ScanQRcode'
import QrReader from 'react-qr-reader'


const handleError = (error) => {
    console.log(error)
}

export default ({handleScan}) => {
    return (
        <div>
        <QrReader
                delay={1500}
                onError={handleError}
                onScan={handleScan}
                style={{ width: '500px' }}
        />
        </div>
    )
}