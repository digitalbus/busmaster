import React, {Component} from 'react'
import {Icon, Menu} from 'semantic-ui-react'
import {Link} from 'react-router-dom'
import {NotificationContainer, NotificationManager} from 'react-notifications'


export default class MenuExampleLabeledIconsVertical extends Component {
    state = {activeItem: 'gamepad', numberOfRequests: 0}

    handleItemClick = (e, {name}) => this.setState({activeItem: name})

    createNotification = () => {
        return (NotificationManager.info('มีคำร้องขอรอบพิเศษใหม่','แจ้งเตือน' ,3000, callback => {
            console.log(this.props.history.push('/approve'))
        }))
    }

    componentDidMount = async () => {
        this.fetchInterval = setInterval(this.fetchData, 10000)
    }

    componentWillUnmount = () => {
        clearInterval(this.fetchInterval)
    }

    fetchData = async () => {
        let {numberOfRequests} = this.state
        let req = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/queue/request/number`)
        req = await req.json()
        if (req > numberOfRequests) {
            console.log('yeah')
            this.setState({numberOfRequests: req})
            this.createNotification()
        } else {
            this.setState({numberOfRequests: req})
        }
    }

    render() {
        const {activeItem} = this.state

        return (
            <Menu icon='labeled' vertical>
                <Link to='/dashboard'><Menu.Item
                    as='div'
                    name='Dashboard'
                    active={activeItem === 'Dashboard'}
                    onClick={this.handleItemClick}
                >
                    <Icon name='area graph'/>
                    Dashboard
                </Menu.Item></Link>

                <Link to='/queue'><Menu.Item
                    as='div'
                    name='Queue'
                    active={activeItem === 'Queue'}
                    onClick={this.handleItemClick}
                >
                    <Icon name='user circle'/>
                    การจัดการคิว
                </Menu.Item></Link>

                <Link to='/approve'><Menu.Item
                    as='div'
                    name='Data'
                    active={activeItem === 'Data'}
                    onClick={this.handleItemClick}
                >
                    {this.state.numberOfRequests > 0 ? (<div style={{
                        backgroundColor: 'red',
                        color: 'white',
                        borderRadius: '17px',
                        width: '17px',
                        top: '7px',
                        right: '103px',
                        position: 'absolute',
                        fontSize: '18px'
                    }}>
                        {this.state.numberOfRequests}
                    </div>) : null}
                    <Icon name='user plus'/>
                    การอนุมัติคิว
                </Menu.Item></Link>

                <Link to='/timetable'><Menu.Item
                    as='div'
                    name='Timetable'
                    active={activeItem === 'Timetable'}
                    onClick={this.handleItemClick}
                >
                    <Icon name='calendar alternate outline'/>
                    การจัดการตารางเวลา
                </Menu.Item></Link>

                <Link to='/semester'><Menu.Item
                    as='div'
                    name='manageSemester'
                    active={activeItem === 'semester'}
                    onClick={this.handleItemClick}
                >
                    <Icon name='calendar check outline'/>
                    การตั้งค่าตารางเวลา
                </Menu.Item></Link>

                <Link to='/driver'><Menu.Item
                    as='div'
                    name='driver'
                    active={activeItem === 'driver'}
                    onClick={this.handleItemClick}
                >
                    <Icon name='drivers license'/>
                    การจัดการรายชื่อคนขับ
                </Menu.Item></Link>

                <Link to='/bus'><Menu.Item
                    as='div'
                    name='Transport'
                    active={activeItem === 'Transport'}
                    onClick={this.handleItemClick}
                >
                    <Icon name='car'/>
                    การจัดการรถรับส่ง
                </Menu.Item></Link>
            </Menu>
        )
    }
}

