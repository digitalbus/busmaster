import React from 'react'
import {Icon, Popup, Table} from 'semantic-ui-react'
import Swal from 'sweetalert2'
import DriverInput from './DriverInput'

export default class extends React.Component {

    state = {
        driver: [],
        isShowModal: false,
        driverId: undefined,
    }

    handleToggleModal = id => () => {

        if (id) {
            this.setState({
                driverId: id
            }, () => {
                console.log('after')
                this.setState({
                    isShowModal: !this.state.isShowModal,
                })
            })
        } else {
            this.setState({
                driverId: undefined,
                isShowModal: !this.state.isShowModal,
            })
        }

    }


    componentDidMount = async () => {
        this.fetchInterval = setInterval(this.fetchData, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.fetchInterval)
    }

    fetchData = async () => {
        let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/bus_driver`)
        let list = await data.json()
        this.setState({driver: list})
    }

    handleDelete = async (driverId) => {
        this.componentDidMount()
        Swal.fire({
            title: 'คุณต้องการลบรายการนี้ใช่หรือไม่ ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then(async (result) => {
            if (result.value) {
                let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/bus_driver/${driverId}`, {
                    method: 'DELETE'
                })
                if (data.ok) {
                    Swal.fire(
                        'ลบสำเร็จ!',
                        'success'
                    )
                    this.componentDidMount()
                }
            }
        })
    }


    render = () => {
        return (
            <div>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>คนที่</Table.HeaderCell>
                            <Table.HeaderCell>เลขรหัสพนักงาน</Table.HeaderCell>
                            <Table.HeaderCell>ชื่อจริง</Table.HeaderCell>
                            <Table.HeaderCell>นามสกุล</Table.HeaderCell>
                            <Table.HeaderCell>ตัวเลือก</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.driver.map(driver => (
                                <Table.Row key={driver.id}>
                                    <Table.Cell>{driver.id}</Table.Cell>
                                    <Table.Cell>{driver.kmuttId}</Table.Cell>
                                    <Table.Cell>{driver.firstName}</Table.Cell>
                                    <Table.Cell>{driver.lastname}</Table.Cell>
                                    <Table.Cell>
                                        <div>
                                            <Popup
                                                trigger={<Icon name='trash alternate'
                                                               style={{float: 'right', color: 'Gray'}}
                                                               onClick={() => this.handleDelete(driver.id)}/>}
                                                content='ลบข้อมูล'
                                                size='mini'
                                            />
                                        </div>
                                        <div>
                                            <Popup
                                                trigger={<Icon
                                                    name='edit outline'
                                                    style={{float: 'right', color: '#DB5A6B'}}
                                                    onClick={this.handleToggleModal(driver.id)}/>}
                                                content='แก้ไขข้อมูล'
                                                size='mini'
                                            />

                                        </div>
                                    </Table.Cell>
                                </Table.Row>
                            )
                        )}
                    </Table.Body>
                </Table>
                {
                    this.state.driverId &&
                    <DriverInput targetDriverId={this.state.driverId} isShowModal={this.state.isShowModal}
                                 toggleModal={this.handleToggleModal} {...this.props}/>
                }

            </div>
        )
    }
}