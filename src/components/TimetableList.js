import React from 'react'
import { Icon, Popup, Table, Button, Checkbox } from 'semantic-ui-react'
import Swal from 'sweetalert2'
import TimetableInput from '../pages/TimetableInput'
import { thisExpression } from '@babel/types'
import PresetAdd from "./PresetAdd"
import Group from '../assets/img/Group.png'




export default class extends React.Component {

    state = {
        isShowModal: false,
        timetableId: undefined
    }

    handleToggleModal = id => () => {
        if (id) {
            this.setState({
                timetableId: id
            }, () => {
                this.setState({
                    isShowModal: !this.state.isShowModal
                })
            })
        } else {
            this.setState({
                timetableId: undefined,
                isShowModal: !this.state.isShowModal
            })
        }
    }


    constructor(props) {
        super(props)
    }

    handleRequest = (route) => {
        console.log('RouteId = ', route)
        this.props.navigation.navigate('Reserve', { "route": route })
    }

    handleDelete = async (timetableid) => {
        this.componentDidMount()
        Swal.fire({
            title: 'คุณต้องการลบรายการนี้ใช่หรือไม่',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(async (result) => {
            if (result.value) {
                let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/timetable/${timetableid}`, {
                    method: 'DELETE'
                }
                )
                if (data.ok) {
                    await Swal.fire(
                        'ลบสำเร็จ!',
                        'success'
                    )
                    await this.props.fetchTimetable()
                }
            }
        })
    }

    componentDidMount = () => {
        console.log('Props', this.props)
    }

    handleClick = (timetable) => {
        this.setState({ isShowModal: false })
        timetable.active = !timetable.active
        console.log('select timetable id', timetable)
        this.handleSave(timetable)
    }

    handleSave = async (timetable) => {
        let res = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/timetable`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(timetable)
        })
        console.log('res', res)
        this.props.fetchTimetable()
    }

    render = () => {
        const timetables = this.props.timetables
        console.log('proppssss', this.props)
        console.log('timetableComponent', timetables)
        console.log('idd', this.state.timetableId)
        if (timetables) {
            return (
                <div>
                    <Table celled style={{ marginTop: 20 }}>
                        <Table.Header fullWidth>
                            <Table.Row>
                                <Table.HeaderCell>วัน</Table.HeaderCell>
                                <Table.HeaderCell>รอบเวลา</Table.HeaderCell>
                                <Table.HeaderCell>ประเภทรถรับ-ส่ง</Table.HeaderCell>
                                <Table.HeaderCell>จำนวนรถพร้อม</Table.HeaderCell>
                                <Table.HeaderCell>ตัวเลือก</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        {timetables.map(timetable => (
                            <Table.Body>
                                <Table.Row>
                                    <Table.Cell>{timetable.day}</Table.Cell>
                                    <Table.Cell>{timetable.time}</Table.Cell>
                                    <Table.Cell>{timetable.busType}</Table.Cell>
                                    <Table.Cell>{timetable.standbyBus} </Table.Cell>
                                    <Table.Cell>
                                        <div>
                                            <Checkbox
                                                toggle
                                                checked={timetable.active}
                                                onClick={() => this.handleClick(timetable)}
                                                label={timetable.active ? 'Active' : 'Inactive'}
                                            />
                                            <Popup
                                                trigger={<Icon name='trash alternate'
                                                    style={{ float: 'right', color: 'Gray' }}
                                                    onClick={() => this.handleDelete(timetable.id)} />}
                                                content='ลบข้อมูล'
                                                size='mini'
                                            />
                                            <Popup
                                                trigger={
                                                    <Icon
                                                        name='edit outline' style={{ float: 'right', color: '#DB5A6B' }}
                                                        onClick={this.handleToggleModal(timetable.id)}
                                                    />}
                                                content='แก้ไขข้อมูล'
                                                size='mini'
                                            />
                                        </div>
                                    </Table.Cell>
                                </Table.Row>
                            </Table.Body>
                        )
                        )
                        }
                    </Table>
                    {this.state.timetableId && (
                        <TimetableInput targetTimetableId={this.state.timetableId}
                            isShowModal={this.state.isShowModal}
                            toggleModal={this.handleToggleModal} {...this.props} />
                    )}
                </div>
            )
        } else {
            return <div>
                <p style={{ textAlign: 'center', marginTop: '10%', marginRight:'7%', fontSize: '20px' }}>
                    <Icon name='hand point up outline' />
                    โปรดเลือกสถานที่
                </p>
                {console.log('No data')}</div>
        }
    }

}