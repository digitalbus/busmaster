import React from 'react'
import {Container, Dropdown} from 'semantic-ui-react'


export default class extends React.Component {

    constructor(props) {
        super(props)
        console.log('proppppp',props)
        this.state = {
            chosenDate: new Date(),
        }
        this.setDate = this.setDate.bind(this)
    }

    setDate(newDate) {
        this.setState({chosenDate: newDate})
    }

    async componentDidMount() {
    }

    render() {

        return (
            <Container>
                <div>
                    <div>
                        <Dropdown
                            style={{width: '25%', fontSize: '15px'}}
                            placeholder='Departure'
                            search selection
                            onChange={this.props.handleDeparterChange}
                            options={this.props.departureOption}
                        />
                        <Dropdown style={{width: '25%', fontSize: '15px'}}
                                  placeholder='Arrival'
                                  search selection
                                  onChange={this.props.handleChooseRoute}
                                  options={this.props.arrivalOption}
                        />
                        <Dropdown style={{width: '25%', fontSize: '15px'}}
                                  placeholder='Day'
                                  search selection
                                  value={this.props.dayValue}
                                  onChange={this.props.handleChooseDay}
                                  options={this.props.dayOption}
                        />

                    </div>
                </div>
            </Container>
        )

    }
}


