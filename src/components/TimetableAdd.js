import React from 'react'
import { Input, Icon, Grid, Modal, Button } from 'semantic-ui-react'
import styled, { css } from 'styled-components'


const ColumnStyled = styled(Grid.Column)`
    display: flex;
    margin-top: -30px;
`
const BorderInput = styled.input
    `
    border-radius: 8px;
    margin: 30px;
    padding: 5px;

`
const saveButton = css`
    border-color: DodgerBlue;
    background-color: DodgerBlue;
    color: white;
`

const cancelButton = css`
    border-color: lightGray;
    background-color: lightGray;
    color: gray;
`
const BorderButton = styled.button
    `   
    fontSize: 15px;
    padding: 10px;
    border-width: 2px;
    border-style: solid;
    border-radius: 8px;

    ${props => props.save && saveButton || cancelButton}
`

export default class extends React.Component {
    state = {
        timetable: {
            time: undefined,
            standbyBus: undefined,
            busType: undefined,
            day: undefined,
            route: {
                id: undefined
            }
        },
        dayInputError: false,
        busTypeInputError: false,
        standbyBusInputError: false,
        timeInputError: false
    }

    handleSave = async (e) => {
        e.preventDefault()
        let { timetable } = this.state
        let { targetRouteId } = this.props
        timetable.route.id = targetRouteId
        timetable.hibernateLazyInitializer = undefined
        let res = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/timetable`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(timetable)
        })
        console.log(res)
        await this.props.fetchTimetable()
        this.props.toggleModal()//กด save แล้วกลับหน้าเดิม
    }

    handleCancel = (e) => {
        this.props.toggleModal()
    }

    handleTimetableChange = (e) => {
        let time = e.target.value
        let timetable = this.state.timetable
        timetable.time = time
        this.setState({ timetable })
        console.log(this.state.timetable)
    }

    handleStandbyBusChange = (e) => {
        let standbyBus = e.target.value
        let timetable = this.state.timetable
        timetable.standbyBus = standbyBus
        this.setState({ timetable })
        console.log(this.state.timetable)
    }

    handleBusTypeChange = (e) => {
        let busType = e.target.value
        let timetable = this.state.timetable
        timetable.busType = busType
        this.setState({ timetable })
        console.log(this.state.timetable)
    }

    handleDayChange = (e) => {
        let day = e.target.value
        let timetable = this.state.timetable
        timetable.day = day
        this.setState({ timetable })
        console.log(this.state.timetable)
    }

    confirmEdit = (e) => {
        let timetable = this.state.timetable
        console.log(timetable)
        if (
            timetable.time != null &&
            timetable.busType != null &&
            timetable.standbyBus != null &&
            timetable.day != null
        ) {
            this.setState({
                dayInputError: false,
                busTypeInputError: false,
                standbyBusInputError: false,
                timeInputError: false
            })
            this.handleSave(e)
        } else {
            if (timetable.day == undefined) this.setState({ dayInputError: true })
            if (timetable.busType == undefined) this.setState({ busTypeInputError: true })
            if (timetable.standbyBus == undefined) this.setState({ standbyBusInputError: true })
            if (timetable.time == undefined) this.setState({ timeInputError: true })
        }
    }


    render = () => (
        <div>
            <Modal open={this.props.isShowModal} onClose={this.props.toggleModal}>
                <Modal.Header>
                    <Icon name='plus square outline' style={{ float: 'left', color: 'gray' }} />
                    เพิ่มข้อมูลตารางเวลา
                </Modal.Header>

                <Modal.Content>
                    <Modal.Description>
                        <Grid columns={2} style={{ width: '540px' }}>
                            <ColumnStyled first>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '30px', marginLeft: '20px' }}>
                                    วัน
                                </p>
                            </ColumnStyled>
                            <ColumnStyled>
                                <Input
                                    style={{ margin: '20px' }}
                                    list='Day'
                                    placeholder='วันจันทร์'
                                    error={this.state.dayInputError}
                                    value={this.state.timetable.day}
                                    onChange={this.handleDayChange}
                                />
                                <datalist id='Day'>
                                    <option value='monday' />
                                    <option value='tuesday' />
                                    <option value='wednesday' />
                                    <option value='thursday' />
                                    <option value='friday' />
                                    <option value='saturday' />
                                    <option value='sunday/holiday' />
                                </datalist>
                            </ColumnStyled>

                            <ColumnStyled>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '30px', marginLeft: '20px' }}>
                                    รอบเวลาออกรถ
                                </p>
                            </ColumnStyled>
                            <ColumnStyled>
                                <Input
                                    style={{ margin: '20px' }}
                                    list='time'
                                    placeholder='07:00'
                                    error={this.state.timeInputError}
                                    value={this.state.timetable.time}
                                    onChange={this.handleTimetableChange}
                                />
                                <datalist id='time'>
                                    <option value='07:00' />
                                    <option value='07:30' />
                                    <option value='08:00' />
                                    <option value='08:30' />
                                    <option value='09:00' />
                                    <option value='09:30' />
                                    <option value='10:00' />
                                    <option value='10:30' />
                                    <option value='11:00' />
                                    <option value='11:30' />
                                    <option value='12:00' />
                                    <option value='12:30' />
                                    <option value='13:00' />
                                    <option value='14:00' />
                                    <option value='15:00' />
                                    <option value='16:00' />
                                    <option value='17:00' />
                                    <option value='18:00' />
                                    <option value='19:00' />
                                    <option value='20:00' />
                                </datalist>
                            </ColumnStyled>

                            <ColumnStyled>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '30px', marginLeft: '20px' }}>
                                    ประเภทรถ
                                </p>
                            </ColumnStyled>
                            <ColumnStyled>
                                <Input
                                    style={{ margin: '20px' }}
                                    list='Bus type'
                                    placeholder='รถบัส'
                                    error={this.state.busTypeInputError}
                                    value={this.state.timetable.busType}
                                    onChange={this.handleBusTypeChange}
                                />
                                <datalist id='Bus type'>
                                    <option value='van' >รถตู้</option>
                                    <option value='bus' >รถบัส</option>
                                </datalist>
                            </ColumnStyled>

                            <ColumnStyled>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '30px', marginLeft: '20px' }}>
                                    จำนวนรถพร้อม
                                </p>
                            </ColumnStyled>
                            <ColumnStyled>
                                <BorderInput
                                    error={this.state.standbyBusInputError}
                                    value={this.state.timetable.standbyBus}
                                    onChange={this.handleStandbyBusChange}
                                    name={'standbyBus'}
                                    type={'number'}
                                    min="1" max="10"
                                    placeholder='1'
                                />
                            </ColumnStyled>
                        </Grid>
                    </Modal.Description>
                </Modal.Content>


                <Modal.Actions>

                    <BorderButton
                        name="cancel"
                        type="reset"
                        onClick={this.handleCancel}
                    >
                        ยกเลิก
                    </BorderButton>

                    <BorderButton
                        name="save"
                        type="submit"
                        onClick={this.confirmEdit}
                        save
                    >
                        บันทึก
                    </BorderButton>
                </Modal.Actions>
            </Modal>
        </div>
    )
}