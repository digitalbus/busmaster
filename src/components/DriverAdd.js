import React from 'react'
import { Input, Icon, Grid, Modal } from 'semantic-ui-react'
import styled, { css } from 'styled-components'

const ColumnStyled = styled(Grid.Column)
    `
    display: flex;
    margin-top: -30px;
`
const BorderInput = styled.input
    `
    border-radius: 8px;
    margin: 30px;
    padding: 5px;

`
const saveButton = css`
    border-color: DodgerBlue;
    background-color: DodgerBlue;
    color: white;
`

const cancelButton = css`
    border-color: lightGray;
    background-color: lightGray;
    color: gray;
`
const BorderButton = styled.button
    `   
    fontSize: 15px;
    padding: 10px;
    border-width: 2px;
    border-style: solid;
    border-radius: 8px;

    ${props => props.save && saveButton || cancelButton}
`

export default class extends React.Component {

    state = {
        driver: {
            kmuttId: undefined,
            firstName: undefined,
            lastname: undefined
        },
        kmuttIdInputError: false,
        firstNameInputError: false,
        lastnameInputError: false
    }

    handleSave = async (event) => {
        let driver = this.state.driver//เรียกข้อมูลมาโดยที่ format ต้องตรงกับ postman
        driver.hibernateLazyInitializer = undefined
        let res = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/bus_driver`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(driver)
        })
        this.setState({driver:{}})
        this.props.toggleModal()
    }

    handleCancel = (e) => {
        this.props.toggleModal()
    }

    handlelKmuttIdChange = (e) => {
        let kmuttId = e.target.value
        let driver = this.state.driver
        driver.kmuttId = kmuttId
        this.setState({ driver })
        console.log(this.state.driver)
    }

    handleFirstNameChange = (e) => {
        let firstName = e.target.value
        let driver = this.state.driver
        driver.firstName = firstName
        this.setState({ driver })
        console.log(this.state.driver)
    }

    handleLastnameChange = (e) => {
        let lastname = e.target.value
        let driver = this.state.driver
        driver.lastname = lastname
        this.setState({ driver })
        console.log(this.state.driver)
    }

    confirmEdit = (e) => {
        e.preventDefault()//ทำให้มันไม่ต้อง reload
        let driver = this.state.driver
        console.log(driver)
        if (
            driver.kmuttId &&
            driver.firstName &&
            driver.lastname
        ) {
            this.setState({
                kmuttIdInputError: false,
                firstNameInputError: false,
                lastnameInputError: false
            })
            this.handleSave()
        } else {
            if (!driver.kmuttId) this.setState({  kmuttIdInputError: true })
            if (!driver.firstName) this.setState({ firstNameInputError: true })
            if (!driver.lastname) this.setState({ lastnameInputError: true })
        }
    }

    render = () => (
        <div>
            <Modal open={this.props.isShowModal} onClose={this.props.toggleModal} >
                <Modal.Header>
                    <Icon name='plus square outline' style={{ float: 'left', color: '#DB5A6B' }} />
                    เพิ่มข้อมูลพนักงาน
                </Modal.Header>

                <Modal.Content>
                    <Modal.Description>
                        <Grid columns={2} style={{ width: '540px' }}>
                            <ColumnStyled>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '25px', marginLeft: '20px' }}>
                                    เลขรหัสพนักงาน:
                                </p>
                            </ColumnStyled>
                            <ColumnStyled>
                                <Input
                                    type="number" 
                                    style={{ marginTop: '25px', marginLeft: '20px' }}
                                    label={{ icon: 'asterisk' }}
                                    labelPosition='right corner'
                                    placeholder='59130500000'
                                    error={this.state.kmuttIdInputError}
                                    value={this.state.driver.kmuttId}
                                    onChange={this.handlelKmuttIdChange}
                                />
                            </ColumnStyled>

                            <ColumnStyled>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '30px', marginLeft: '20px' }}>
                                    ชื่อจริง:
                                </p>
                            </ColumnStyled>
                            <ColumnStyled>
                                <Input
                                    style={{ marginTop: '25px', marginLeft: '20px' }}
                                    label={{ icon: 'asterisk' }}
                                    labelPosition='right corner'
                                    placeholder='ณเดชน์'
                                    error={this.state.firstNameInputError}
                                    value={this.state.driver.firstName}
                                    onChange={this.handleFirstNameChange}
                                />
                            </ColumnStyled>

                            <ColumnStyled>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '20px', marginLeft: '20px' }}>
                                    นามสกุล:
                                </p>
                            </ColumnStyled>
                            <ColumnStyled>
                                <Input
                                    style={{ marginTop: '25px', marginLeft: '20px' }}
                                    label={{ icon: 'asterisk' }}
                                    labelPosition='right corner'
                                    placeholder='คูกิมิยะ'
                                    error={this.state.lastnameInputError}
                                    value={this.state.driver.lastname}
                                    onChange={this.handleLastnameChange}
                                />
                            </ColumnStyled>
                        </Grid>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <BorderButton
                        name="cancel"
                        type="reset"
                        onClick={this.handleCancel}
                    >
                        ยกเลิก
                    </BorderButton>
                    <BorderButton
                        name="save"
                        type="submit"
                        onClick={this.confirmEdit}
                        save
                    >
                        บันทึก
                    </BorderButton>
                </Modal.Actions>
            </Modal>
        </div>
    )
}