import React from 'react'
import OptionQueueTabs from '../OptionQueueTabs/index'
import {Segment} from 'semantic-ui-react'
import Swal from 'sweetalert2'

export default class extends React.Component {

    state = {
        route: {},
        loading: true,
    }

    componentWillMount = () => {
        clearInterval()
    }

    componentWillUnmount = () => {
        clearInterval(this.state.timer)
    }

    componentDidMount = async () => {
        const {tripId} = this.props
        let trip = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/trip/${tripId}`)
        trip = await trip.json()
        await this.setState({trip})
        let timer = await setInterval(this.intervalFunction, 3500)
        this.fetchQueueStatus(trip)
    }

    fetchAnyTrip = async (id) => {
        let anyTripForThisTimetable
        try {
            anyTripForThisTimetable = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/trip/timetable/${id}`)
            await anyTripForThisTimetable.json()
        } catch (error) {
            anyTripForThisTimetable = null
        }
        this.setState({trip: anyTripForThisTimetable})
    }

    intervalFunction =  async () => {
        const {trip} = this.state
        await this.fetchQueueStatus(trip)
    }

    fetchQueueStatus = async (trip) => {
        if (trip.id) {
            let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/queue/trip/status/${trip.id}`)
            data = await data.json()
            let booked = data.filter(queue => {
                if (queue.queueType == 'booked') {
                    return queue
                } else if (queue.queueType == 'reserved') {
                    return queue
                } else {
                    return null
                }
            })
            let checkedIn = data.filter(queue => queue.queueType == 'checkedin' ? queue : null)
            this.setState({reserved: booked, checkedIn})
            console.log('reserved', data)
        }
    }

    bookedList = () => {
        let {reserved} = this.state
        if (reserved) {
            return reserved.map(reserve => (
                <div
                    key={reserve.id}>{`${reserve.passenger.firstName} ${reserve.passenger.lastName}  (${reserve.queueType})`}</div>
            ))
        } else {
            return (<div>- no data -</div>)
        }
    }

    checkinList = () => {
        let {checkedIn} = this.state
        if (checkedIn) {
            return checkedIn.map(checkin => (
                <div key={checkin.id}>{`${checkin.passenger.firstName} ${checkin.passenger.lastName} `} </div>
            ))
        } else {
            return (<div>- no data -</div>)
        }
    }

    handleScan = async (queueId) => {
        let {checkedIn} = this.state
        if (queueId) {
            let data = {
                passenger: {
                    firstName: 'none',
                    lastName: 'none'
                }
            }
            data = await fetch(`${process.env.REACT_APP_BUSMASTER_QUEUESERVICE}/queue/checkin/${queueId}`)
            if (data.status == 200) {
                data = await data.json()
                await Swal.fire({
                    type: 'success',
                    title: `คิวของ ${data.passenger.firstName}  ${data.passenger.lastName} เช็คอินสำเร็จ`,
                    showConfirmButton: false,
                    timer: 1500
                })
                checkedIn.push(data)
                this.setState({loading: true, checkedIn})
                this.componentDidMount()
            } else if (data.status == 404) {
                data = await data.json()
                await Swal.fire({
                    type: 'error',
                    title: `คิว ${queueId} เช็คอินไม่สำเร็จ`,
                    showConfirmButton: false,
                    timer: 1500
                })
            } else {
                data = await data.json()
                await Swal.fire({
                    type: 'error',
                    title: `เกิดข้อผิดพลาด`,
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        }
    }

    render = () => {
        return (
            <div>
                {this.state.trip && this.state.trip.busStamp ? (<div
                        style={{fontSize: '32px', paddingTop: ' 10px', textAlign: 'center'}}>รอบนี้รถออกไปแล้ว</div>) :
                    <OptionQueueTabs trip={this.state.trip} fetchAnyTrip={this.fetchAnyTrip} timetable={this.state.timetable}
                                     handleScan={this.handleScan}/>
                }
                <Segment.Group horizontal>
                    <Segment>
                        <h3>รายชื่อคนจอง</h3>
                        {this.bookedList()}
                    </Segment>
                    <Segment><h3>รายชื่อคนเช็คอิน</h3>
                        {this.checkinList()}
                    </Segment>
                </Segment.Group>
            </div>
        )
    }
}
