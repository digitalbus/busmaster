import React from 'react'
import { Input, Icon, Grid, Modal } from 'semantic-ui-react'
import styled, { css } from 'styled-components'

const ColumnStyled = styled(Grid.Column)
    `
    display: flex;
    margin-top: -30px;
`
const BorderInput = styled.input
    `
    border-radius: 8px;
    margin: 30px;
    padding: 5px;

`
const saveButton = css`
    border-color: DodgerBlue;
    background-color: DodgerBlue;
    color: white;
`

const cancelButton = css`
    border-color: lightGray;
    background-color: lightGray;
    color: gray;
`
const BorderButton = styled.button
    `   
    fontSize: 15px;
    padding: 10px;
    border-width: 2px;
    border-style: solid;
    border-radius: 8px;

    ${props => props.save && saveButton || cancelButton}
`

export default class extends React.Component {

    state = {
        bus: {
            licencePlate: undefined,
            busType: undefined,
            seat: undefined
        },
        licencePlateInputError: false,
        busTypeInputError: false,
        seatInputError: false
    }

    componentDidMount = async () => {
        let param = this.props.targetBusId
        let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/bus/${param}`)
        data = await data.json()
        this.setState({ bus: data })
    }

    handleSave = async (event) => {
        let bus = this.state.bus//เรียกข้อมูลมาโดยที่ format ต้องตรงกับ postman
        bus.hibernateLazyInitializer = undefined
        let res = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/bus`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(bus)
        })

        console.log(res)
        this.props.toggleModal()()//กด save แล้วกลับหน้าเดิม
    }

    handleCancel = (e) => {
        this.props.toggleModal()()
    }

    handlelicencePlateChange = (e) => {
        let licencePlate = e.target.value
        let bus = this.state.bus
        bus.licencePlate = licencePlate
        this.setState({ bus })
        console.log(this.state.bus)
    }

    handleBusTypeChange = (e) => {
        let busType = e.target.value
        let bus = this.state.bus
        bus.busType = busType
        this.setState({ bus })
        console.log(this.state.bus)
    }

    handleSeatChange = (e) => {
        let seat = e.target.value
        let bus = this.state.bus
        bus.seat = seat
        this.setState({ bus })
        console.log(this.state.bus)
    }

    confirmEdit = (e) => {
        e.preventDefault()//ทำให้มันไม่ต้อง reload
        let bus = this.state.bus
        console.log(bus)
        if (
            bus.licencePlate &&
            bus.busType &&
            bus.seat
        ) {
            this.setState({
                licencePlateInputError: false,
                busTypeInputError: false,
                seatInputError: false
            })
            this.handleSave()
        } else {
            if (!bus.licencePlate) this.setState({ licencePlateInputError: true })
            if (!bus.busType) this.setState({ busTypeInputError: true })
            if (!bus.seat) this.setState({ seatInputError: true })
        }
    }

    render = () => (
        <div>
            <Modal open={this.props.isShowModal} onClose={this.props.toggleModal} >
                <Modal.Header>
                    <Icon name='edit outline' style={{ float: 'left', color: '#DB5A6B' }} />
                    แก้ไขข้อมูลรถ
                </Modal.Header>

                <Modal.Content>
                    <Modal.Description>
                        <Grid columns={2} style={{ width: '540px' }}>
                            <ColumnStyled>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '25px', marginLeft: '20px' }}>
                                    ทะเบียนรถ:
                                </p>
                            </ColumnStyled>
                            <ColumnStyled>
                                <Input
                                    style={{ marginTop: '25px', marginLeft: '20px' }}
                                    label={{ icon: 'asterisk' }}
                                    labelPosition='right corner'
                                    placeholder='กท 123'
                                    error={this.state.licencePlateInputError}
                                    value={this.state.bus.licencePlate}
                                    onChange={this.handlelicencePlateChange}
                                />
                            </ColumnStyled>

                            <ColumnStyled>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '30px', marginLeft: '20px' }}>
                                    ประเภทของรถ:
                                </p>
                            </ColumnStyled>
                            <ColumnStyled>
                                <Input
                                    style={{ margin: '20px' }}
                                    list='transport type'
                                    placeholder='รถตู้'
                                    error={this.state.busTypeInputError}
                                    value={this.state.bus.busType}
                                    onChange={this.handleBusTypeChange} />
                                <datalist id='transport type'>
                                    <option value='van' >รถตู้</option>
                                    <option value='bus' >รถบัส</option>
                                </datalist>
                            </ColumnStyled>

                            <ColumnStyled>
                                <p style={{ fontSize: '18px', justifySelf: 'center', marginTop: '20px', marginLeft: '20px' }}>
                                    จำนวนที่นั่ง:
                                </p>
                            </ColumnStyled>
                            <ColumnStyled>
                                <input
                                    style={{ margin: '20px' }}
                                    error={this.state.seatInputError}
                                    value={this.state.bus.seat}
                                    onChange={this.handleSeatChange}
                                    name={'seat'}
                                    type={'number'}
                                    min="1" max="30"
                                />
                            </ColumnStyled>
                        </Grid>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <BorderButton
                        name="cancel"
                        type="reset"
                        onClick={this.handleCancel}
                    >
                        ยกเลิก
                    </BorderButton>
                    <BorderButton
                        name="save"
                        type="submit"
                        onClick={this.confirmEdit}
                        save
                    >
                        บันทึก
                    </BorderButton>
                </Modal.Actions>
            </Modal>
        </div>
    )
}