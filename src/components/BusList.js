import React from 'react'
import { Card, Icon, Grid, Container, Label, Menu, Table, Popup } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import BusInput from './BusInput'

export default class extends React.Component {

    state = {
        bus: [],
        isShowModal: false,
        busId: undefined,
    }

    handleToggleModal = id => () => {

        if (id) {
            this.setState({
                busId: id
            }, () => {
                console.log('after')
                this.setState({
                    isShowModal: !this.state.isShowModal,
                })
            })
        } else {
            this.setState({
                busId: undefined,
                isShowModal: !this.state.isShowModal,
            })
        }

    }


    componentDidMount = async () => {
        this.fetchInterval = setInterval(this.fetchData, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.fetchInterval)
    }

    fetchData = async () => {
        let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/bus`)
        let list = await data.json()
        this.setState({ bus: list })
    }

    handleDelete = async (busid) => {
        this.componentDidMount()
        Swal.fire({
            title: 'คุณต้องการลบรายการนี้ใช่หรือไม่ ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then(async (result) => {
            if (result.value) {
                let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/bus/${busid}`, {
                    method: 'DELETE'
                })
                if (data.ok) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                    this.componentDidMount()
                }
            }
        })
    }


    render = () => {
        return (
            <div>
                <Table celled >
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>คันที่</Table.HeaderCell>
                            <Table.HeaderCell>ทะเบียนรถ</Table.HeaderCell>
                            <Table.HeaderCell>ประเภทรถ</Table.HeaderCell>
                            <Table.HeaderCell>จำนวนที่นั่ง</Table.HeaderCell>
                            <Table.HeaderCell>ตัวเลือก</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.bus.map(bus => (
                            <Table.Row key={bus.id}>
                                <Table.Cell>{bus.id}</Table.Cell>
                                <Table.Cell>{bus.licencePlate}</Table.Cell>
                                <Table.Cell>{bus.busType}</Table.Cell>
                                <Table.Cell>{bus.seat}</Table.Cell>
                                <Table.Cell>
                                    <div>
                                        <Popup
                                            trigger={<Icon name='trash alternate' style={{ float: 'right', color: 'Gray' }}
                                                onClick={() => this.handleDelete(bus.id)} />}
                                            content='ลบข้อมูล'
                                            size='mini'
                                        />
                                    </div>
                                    <div>
                                            <Popup
                                                trigger={<Icon
                                                                name='edit outline'
                                                                style={{ float: 'right', color: '#DB5A6B' }}
                                                                onClick={this.handleToggleModal(bus.id)} />}
                                                content='แก้ไขข้อมูล'
                                                size='mini'
                                            />

                                    </div>
                                </Table.Cell>
                            </Table.Row>
                        )
                        )}
                    </Table.Body>
                </Table>
                {
                    this.state.busId && <BusInput targetBusId={this.state.busId} isShowModal={this.state.isShowModal}
                                                                    toggleModal={this.handleToggleModal} {...this.props}/>
                }

            </div>
        )
    }
}