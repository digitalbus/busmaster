import React from 'react'
import {Grid, Modal, Button, Dropdown} from "semantic-ui-react"
import styled, { css } from 'styled-components'
import { Input } from 'semantic-ui-react'


const ColumnStyled = styled(Grid.Column)`
`


export default class extends React.Component {



    constructor(props) {
        super(props)
        this.state = {
            buses: [],
            drivers: [],
        }
    }

    async componentDidMount() {
        let drivers = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/bus_driver`)
        drivers = await drivers.json()
        let buses = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/bus`)
        buses = await buses.json()
        this.setState({drivers,buses})
        console.log(buses,drivers,this.props.timetable)
    }

    approveRequests = async () => {
        const trip = {
            "driver": {'id': this.state.selectedDriver},
            "bus": {'id': this.state.selectedBus},
            "timetable": this.props.timetable,
        }
        let res = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/queue/approve`, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(trip)
        })
        this.props.fetch()
        this.props.toggleModal()
    }

    cancel = () => {
        this.props.toggleModal()
    }

    driverChange  = (event,pick) => {
        this.setState({selectedDriver:pick.value})
    }

    busChange  = (event,pick) => {
        this.setState({selectedBus:pick.value})
    }

    convertBusToDropdown = () => {
        let {buses} = this.state
        buses.map((bus,key) => {
        buses[key] = {
            ...bus,
            text:`${bus.licencePlate} (${bus.seat} seat)`,
            value: bus.id
        }
        })
        console.log('buses' ,buses)
        return buses
    }
    convertDriverToDropdown = () => {
        let {drivers} = this.state
        drivers.map((driver, key) => {
        drivers[key] = {
            ...driver,
            text:`${driver.firstName} ${driver.lastname}`,
            value: driver.id
        }
        })
        return drivers
    }


    render = () => (
        <>
            <Modal open={this.props.isShowModal} onClose={this.props.toggleModal}>
                <Modal.Header>
                    เพิ่มรอบเพื่อรองรับคิวพิเศษ
                </Modal.Header>
                <Modal.Content>
                    <Modal.Description>
                        <Grid columns={2} style={{ width: '540px' }}>
                            <ColumnStyled>
                               คนขับ:
                            </ColumnStyled>
                            <ColumnStyled >
                                <Dropdown
                                    onChange={this.driverChange}
                                    placeholder='Select Friend'
                                    options={this.convertDriverToDropdown()}
                                />
                            </ColumnStyled>
                            <ColumnStyled>
                                รถ:
                            </ColumnStyled>
                            <ColumnStyled >
                                <Dropdown
                                    onChange={this.busChange}
                                    placeholder='Select Friend'
                                    options={this.convertBusToDropdown()}
                                />
                            </ColumnStyled>
                            <ColumnStyled>
                                รอบ:
                            </ColumnStyled>
                            <ColumnStyled >
                                <Input value={` ${this.props.timetable.route.departure} => ${this.props.timetable.route.arrival} ${this.props.timetable.time}`} disabled/>
                            </ColumnStyled>
                        </Grid>
                        <Modal.Actions>
                            <Button positive onClick={this.approveRequests} >Approve</Button>
                            <Button negative onClick={this.cancel} >Cancel</Button>
                        </Modal.Actions>
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        </>
    )
}
