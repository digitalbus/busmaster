import React from 'react'
import {Icon, Popup, Table, Button} from "semantic-ui-react"

class RequestQueueList extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <Table celled style={{marginTop: 20}}>
                    <Table.Header fullWidth>
                        <Table.Row>
                            <Table.HeaderCell>วัน</Table.HeaderCell>
                            <Table.HeaderCell>เวลา</Table.HeaderCell>
                            <Table.HeaderCell>ประเภทรถที่กำหนด</Table.HeaderCell>
                            <Table.HeaderCell>จำนวนรถ</Table.HeaderCell>
                            <Table.HeaderCell>คนจอง</Table.HeaderCell>
                            <Table.HeaderCell>คิวพิเศษ</Table.HeaderCell>
                            <Table.HeaderCell>จัดการ</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {this.props.requestQueues.map(requestQueue => (
                            <Table.Row>
                                <Table.Cell>{requestQueue.day}</Table.Cell>
                                <Table.Cell>{requestQueue.time}</Table.Cell>
                                <Table.Cell>{requestQueue.busType}</Table.Cell>
                                <Table.Cell>{requestQueue.standbyBus}</Table.Cell>
                                <Table.Cell>{requestQueue.currentActiveQueue}</Table.Cell>
                                <Table.Cell>{requestQueue.currentRequestQueue}</Table.Cell>
                                <Table.Cell>
                                    <Button.Group>
                                        <Button onClick={() => this.props.selectTimetable(requestQueue)} positive>approve</Button>
                                        <Button.Or />
                                        <Button onClick={() => this.props.cancel(requestQueue)} >reject</Button>
                                    </Button.Group>
                                </Table.Cell>
                            </Table.Row>
                        ))}
                    </Table.Body>
                </Table>
            </div>
        )
    }
}

export default RequestQueueList
