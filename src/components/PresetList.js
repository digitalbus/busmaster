import React from 'react'
import { Button, Icon, Popup, Table, List, Grid } from 'semantic-ui-react'
import Swal from 'sweetalert2'
import PresetInput from './PresetInput'

export default class extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            preset: [],
            isShowModal: false,
            presetId: undefined,
        }
    }

    handleToggleModal = id => () => {
        console.log('handleToggleModal')
        if (id) {
            console.log('haveId')
            this.setState({
                presetId: id
            }, () => {
                console.log('after')
                this.setState({
                    isShowModal: !this.state.isShowModal,
                })
            })
        } else {
            console.log(':(')
            this.setState({
                presetId: undefined,
                isShowModal: !this.state.isShowModal,
            })
        }

    }


    componentDidMount = () => {
        console.log('props from manage',this.props.presets)
        this.setState({preset:this.props.presets})
    }

    fetchData = async () => {
        let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/preset`)
        let list = await data.json()
        this.setState({ preset: list })
    }

    handleIsActive = async (presetId) => {
        this.componentDidMount()
        Swal.fire({
            title: 'คุณต้องการเลือกใช้กลุ่มตารางเวลานี้ใช่หรือไม่ ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก',
            preConfirm: (login) => {
                return fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/preset/active/${presetId}`)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error(response.statusText)
                        }
                        return response.json()
                    })
                    .catch(error => {
                        Swal.showValidationMessage(
                            `Request failed: ${error}`
                        )
                    })
            },
        }).then(async result => {
            await Swal.fire(
                'Active สำเร็จ!',
                'กลุ่มของตารางเวลาเรียกใช้เเล้ว',
                'success'
            )
        })

    }

    handleDelete = async (presetId) => {
        this.componentDidMount()
        Swal.fire({
            title: 'คุณต้องการลบรายการนี้ใช่หรือไม่ ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then(async (result) => {
            if (result.value) {
                let data = await fetch(`${process.env.REACT_APP_BUSMASTER_SYSTEMSERVICE}/system/preset/${presetId}`, {
                    method: 'DELETE'
                })
                if (data.ok) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                    this.componentDidMount()
                }
            }
        })
    }

    render = () => {
        return (
            <div>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>ลำดับที่</Table.HeaderCell>
                            <Table.HeaderCell>ACTIVE</Table.HeaderCell>
                            <Table.HeaderCell>ชื่อกลุ่มตารางเวลา</Table.HeaderCell>
                            <Table.HeaderCell>รายละเอียด</Table.HeaderCell>
                            <Table.HeaderCell>ตารางเวลาที่ถูกเลือก</Table.HeaderCell>
                            <Table.HeaderCell>ตัวเลือก</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.props.presets.map((preset, key) => (
                            <Table.Row key={preset.id}>
                                <Table.Cell>{key + 1}</Table.Cell>
                                <Table.Cell>
                                    <Button active
                                        onClick={() => this.handleIsActive(preset.id)}
                                    >
                                        Active
                                        </Button>
                                </Table.Cell>
                                <Table.Cell>{preset.namePreset}</Table.Cell>
                                <Table.Cell>{preset.description}</Table.Cell>
                                <Table.Cell>
                                    {preset.presetTimetables.map((timetable, index) => (
                                        <List.Item
                                            key={timetable.id}
                                            
                                        >    
                                            <Grid 
                                                columns={3}
                                                style={{textAlign:'center' }}
                                            >
                                                <Grid.Column style={{ padding:'7px'}}>
                                                    {`${timetable.route.departure} `}
                                                    <Icon name="arrow right" />
                                                    {` ${timetable.route.arrival}`}
                                                </Grid.Column>
                                                
                                                <Grid.Column style={{ padding:'7px'}}>
                                                    {`${timetable.day}`} 
                                                </Grid.Column>
                                                
                                                <Grid.Column style={{ padding:'7px', marginRight:'-10px'}}>
                                                    {`${timetable.time}`}
                                                </Grid.Column>
                                            </Grid>
                                        </List.Item>
                                    ))}
                                </Table.Cell>
                                <Table.Cell>
                                    <div>
                                        <Popup
                                            trigger={<Icon name='trash alternate'
                                                style={{ float: 'right', color: 'Gray' }}
                                                onClick={() => this.handleDelete(preset.id)} />}
                                            content='ลบข้อมูล'
                                            size='mini'
                                        />
                                    </div>
                                    <div>
                                        <Popup
                                            trigger={<Icon
                                                name='edit outline'
                                                style={{ float: 'right', color: '#DB5A6B' }}
                                                onClick={this.handleToggleModal(preset.id)} />}
                                            content='แก้ไขข้อมูล'
                                            size='mini'
                                        />

                                    </div>
                                </Table.Cell>
                            </Table.Row>
                        )
                        )}
                    </Table.Body>
                </Table>
                {
                    this.state.presetId && <PresetInput targetPresetId={this.state.presetId}
                        isShowModal={this.state.isShowModal}
                        toggleModal={this.handleToggleModal}
                        {...this.props} />
                }
            </div>
        )
    }
}