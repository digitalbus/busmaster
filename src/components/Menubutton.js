import React from 'react'
import { Header, Image } from 'semantic-ui-react'
import { Icon } from 'semantic-ui-react'


const Menubutton = ({image,text}) => (
  <div style={{backgroundColor:'whiteSmoke',
              margin:'12px',
              padding:'48px',
              textAlign:'center',
              size:'32px',
              fontSize:'25px',
              color:'salmon'}} as='h2'>
    <Icon disabled name={image} /> {text}
  </div>

  
)

export default Menubutton;
